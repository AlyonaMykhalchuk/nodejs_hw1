const http = require('http');
const fs = require('fs');
const url = require('url');
const ERROR_CODE = 400;
const SUCCESS_CODE = 200;
const PORT = 8080;

module.exports = () => {
    const createLog = (message) => {
        return {
            message: message,
            time: new Date().getTime()
        }
    };
    const checkLog = () => {
        if (!fs.existsSync('./logs/logs.json')) {
            return {
                logs: [],
            };
        } else {
            return JSON.parse(fs.readFileSync('./logs/logs.json', 'utf8'));
        }

    };
    const getFilteredLogs = (content, query) => {
        let {from, to} = query;
        if (query.from && query.to) {
            content.logs = content.logs.filter((obj) => obj.time < Number(to) && obj.time > Number(from));
            return content
        } else {
            return content
        }
    };
    const isExists = (path) => {
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }
    };
    const saveLogs = (message) => {
        let newLogs, content, newLog;
        newLog = createLog(message);
        isExists('./logs');
        content = checkLog();
        content.logs.push(newLog);
        newLogs = JSON.stringify(content, null, ' ');

        fs.writeFile('./logs/logs.json', newLogs, (err) => {
            if (err) {
                throw err;
            }
        })
    };

    http.createServer(function (request, response) {
            const {pathname, query} = url.parse(request.url, true);
            const {filename, content} = query;
            if (request.method === 'POST') {
                if (!filename || !content) {
                    response.writeHead(ERROR_CODE, {'Content-type': 'text/html'});
                    response.end('Can\'t create file with incorrect params');
                    return
                }
                if (fs.existsSync(`./file/${query.filename}`)) {
                    response.writeHead(ERROR_CODE, {'Content-type': 'text/html'});
                    response.end('File with such filename was created earlier');
                    return
                }
                isExists('./file');

                fs.writeFile(`./file/${query.filename}`, `${query.content}`, (err) => {
                    if (err) {
                        response.writeHead(ERROR_CODE, {'Content-type': 'text/html'});
                        response.end(`File '${pathname}' wasn't save`)
                    }
                    response.writeHead(SUCCESS_CODE, {'Content-type': 'text/html'});
                    response.end(`${query.content} was saved in ${query.filename}`);
                    saveLogs(`File '${query.filename}' was saved`)
                });
            } else {
                if (request.method === 'GET' && pathname === '/logs') {
                    let filteredLogs;
                    fs.readFile('./logs/logs.json', 'utf8', (err, content) => {
                        if (err) {
                            response.writeHead(ERROR_CODE, {'Content-type': 'text/html'});
                            response.end(`Can't read '${pathname}'`);
                            return;
                        }
                        response.writeHead(SUCCESS_CODE, {'Content-type': 'application/json'});
                        filteredLogs = checkLog(content);
                        filteredLogs = getFilteredLogs(filteredLogs, query);
                        response.end(JSON.stringify(filteredLogs));
                        saveLogs(`File '${pathname}' was read`);
                    })
                } else {
                    fs.readFile(`./${pathname}`, 'utf8', (err, content) => {
                        if (err) {
                            response.writeHead(ERROR_CODE, {'Content-type': 'text/html'});
                            response.end(`No file with name '${pathname}'`);
                            return;
                        }
                        response.writeHead(SUCCESS_CODE, {'Content-type': 'text/html'});
                        response.end(`${content}`);
                        saveLogs(`File '${pathname}' was read`);
                    });
                }
            }
        }
    ).listen(process.env.PORT || PORT, () => console.log('Server has been started on localhost:8080'))
};

